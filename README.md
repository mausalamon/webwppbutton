# How to use #

Insert the tag below before the close tag </body>

	<script type="text/javascript">
	(function () {
		var options = {
			number: "_YOUR_CUSTOM_PHONE",
			text: "_YOUR_CUSTOM_TEXT"
		};
		
		var proto = document.location.protocol, host = "uzzye.com/js/vendor", url = proto + "//" + host;
		var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/UzWhatsAppButton.min.js';
		s.onload = function () {
			UzWhatsAppButton.init(host, proto, options);
		};
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	})();
	</script>